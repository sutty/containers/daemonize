FROM registry.0xacab.org/sutty/containers/sdk:latest
MAINTAINER "f <f@sutty.nl>"

RUN apk update
RUN cp /home/builder/.abuild/*.pub /etc/apk/keys/

WORKDIR /home/builder
COPY ./APKBUILD .
RUN chown -R builder APKBUILD

USER builder
RUN abuild checksum
RUN abuild -r

USER root
